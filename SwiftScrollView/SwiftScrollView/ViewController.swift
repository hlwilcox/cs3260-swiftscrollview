//
//  ViewController.swift
//  SwiftScrollView
//
//  Created by Heather Wilcox on 2/13/18.
//  Copyright © 2018 Heather Wilcox. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let sv = UIScrollView(frame: CGRect(x:0, y:0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        sv.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(sv)
        
        let view1 = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view1.backgroundColor = .yellow
        view1.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        sv.addSubview(view1)
        
        let view2 = UIView(frame: CGRect(x: self.view.frame.size.width, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view2.backgroundColor = .orange
        view2.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin]
        sv.addSubview(view2)
        
        let view3 = UIView(frame: CGRect(x: self.view.frame.size.width * 2, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view3.backgroundColor = .blue
        view3.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin]
        sv.addSubview(view3)
        
        let view4 = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view4.backgroundColor = .red
        view4.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin]
        sv.addSubview(view4)
        
        let view5 = UIView(frame: CGRect(x: 0, y: self.view.frame.size.height * 2, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view5.backgroundColor = .green
        view5.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin]
        sv.addSubview(view5)
        
        let view6 = UIView(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height * 2, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view6.backgroundColor = .purple
        view6.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin]
        sv.addSubview(view6)
        
        let view7 = UIView(frame: CGRect(x: self.view.frame.size.width, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view7.backgroundColor = .black
        view7.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin]
        sv.addSubview(view7)
        
        let view8 = UIView(frame: CGRect(x: self.view.frame.size.width * 2, y: self.view.frame.size.height * 2, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view8.backgroundColor = .brown
        view8.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin]
        sv.addSubview(view8)
        
        let view9 = UIView(frame: CGRect(x: self.view.frame.size.width * 2, y: self.view.frame.size.height, width: self.view.frame.size.width, height: self.view.frame.size.height))
        view9.backgroundColor = .cyan
        view9.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin]
        sv.addSubview(view9)
        
        sv.contentSize = CGSize(width: self.view.frame.size.width * 3, height: self.view.frame.size.height * 3)
        sv.isPagingEnabled = true;
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
